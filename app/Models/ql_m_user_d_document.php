<?php

namespace App\Models;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ql_m_user_d_document extends Model
{
    use HasFactory, ModelTrait;
    protected $table = 'ql_m_user_d_document';
    protected $guarded = ['id','created_at','updated_at'];

    protected $joins = [];

    public $details = [];

    public function creatingRules()
    {
        return [
            'jenis' => "required|string|max:15",
            'no_doc' => "required|string|max:50",
            'date_active' => "nullable|date",
            'is_permanent' => "nullable|boolean",
            'image_url' => "required|string|max:255"
        ];
    }

    public function updatingRules( int $id ): array
    {
        return $this->creatingRules();
    }

    public function onCreating( $model )
    {
        $model->ql_m_user_id = $this->getUserId();
        $model->created_by = $this->getUserName();
        $model->status = 'ACTIVE';
    }

    public function onCreated( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdating( $model )
    {
        $model->updated_by = $this->getUserName();
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdated( $model )
    {
        //
    }

    public function onRetrieved( $model )
    {
        //
    }

    public function onDeleting($model)
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is created or updated - even if the model's attributes have not been changed
     */
    public function onSaving( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is created or updated - even if the model's attributes have not been changed
     */
    public function onSaved( $model )
    {
        //
    }
}
