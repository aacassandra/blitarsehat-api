<?php

namespace App\Models;

use App\Traits\ModelTrait;
use http\Env\Response;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ql_m_user_d_asuransi extends Model
{
    use HasFactory, ModelTrait;
    protected $table = 'ql_m_user_d_asuransi';
    protected $guarded = ['id','created_at','updated_at'];

    protected $joins = [];

    public $details = [];

    public function creatingRules()
    {
        $request = app()->request;
        return [
            'name' => "required|string|max:50",
            'no_card' => "required|string|max:50",
            'cara_bayar' => "required|string|max:20",
            'grade' => "required|string|max:20",
            'image_url' => "required|string|max:255"
        ];
    }

    public function updatingRules( int $id ): array
    {
        return $this->creatingRules();
    }

    public function onCreating( $model )
    {
        $check = $this->where([
            ['name', '=', $model->name],
            ['no_card', '=', $model->no_card]
        ]);

        if ($check->exists()) {
            abort(422, 'The name and no_card has already been taken.');
        }

        $model->ql_m_user_id = $this->getUserId();
        $model->created_by = $this->getUserName();
        $model->status = 'ACTIVE';
    }

    public function onCreated( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdating( $model )
    {
        $check = $this->where([
            ['name', '=', $model->name],
            ['no_card', '=', $model->no_card],
            ['id', '<>', $this->autoDecrypt($model->id)]
        ]);

        if ($check->exists()) {
            abort(422, 'The name and no_card has already been taken.');
        }

        $model->updated_by = $this->getUserName();
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdated( $model )
    {
        //
    }

    public function onRetrieved( $model )
    {
        //
    }

    public function onDeleting($model)
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is created or updated - even if the model's attributes have not been changed
     */
    public function onSaving( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is created or updated - even if the model's attributes have not been changed
     */
    public function onSaved( $model )
    {
        //
    }
}
