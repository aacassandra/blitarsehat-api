<?php

namespace App\Models;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class set_mas_gen extends Model
{
    //.
    use HasFactory, ModelTrait, SoftDeletes;
    protected $table = 'ql_m_user';
    protected $guarded = ['id','created_at','updated_at'];
}
