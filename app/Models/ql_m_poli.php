<?php

namespace App\Models;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ql_m_poli extends Model
{
    use HasFactory, ModelTrait, SoftDeletes;
    protected $table = 'ql_m_poli';
    protected $guarded = ['id','created_at','updated_at'];

    protected $joins = [];

    public $details = [];

    public function creatingRules()
    {
        return [
            'code' => "required|string|unique:ql_m_poli,code",
            'name' => "required|string",
            'tags' => "nullable|string|max:50",
            'note' => "nullable|string|max:255",
        ];
    }

    public function updatingRules( int $id ): array
    {
        $rules = $this->creatingRules(); // menyamakan dengan create, tapi diubah sedikit
        foreach( $rules as $key => $value ){
            if( $key == 'code' ){
                $rules[$key].=",$id"; //   untuk validasi unique selain diri sendiri
            }
        }
        return $rules;
    }

    public function onCreating( $model )
    {
        $model->created_by = $this->getUserName();
        $model->status = 'ACTIVE';
    }

    public function onCreated( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdating( $model )
    {
        $model->updated_by = $this->getUserName();
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdated( $model )
    {
        //
    }

    public function onRetrieved( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is creating or updating
     */
    public function onSaving( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is created or updated
     */
    public function onSaved( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is deleting
     */
    public function onDeleting( $model )
    {
        $model->deleted_by = $this->getUserName();
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is deleted
     */
    public function onDeleted( $model )
    {
        //
    }
}
