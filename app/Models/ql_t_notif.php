<?php

namespace App\Models;

use App\Core\Firebase;
use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ql_t_notif extends Model
{
    use HasFactory, ModelTrait, SoftDeletes;
    protected $table = 'ql_t_notif';
    protected $guarded = ['id','created_at','updated_at'];

    protected $joins = [];

    public $details = [];

    public function creatingRules()
    {
        return [
            'jenis' => "required|string|max:20",
            'navigate_to' => "nullable|string|max:255",
            'title' => "required|string|max:30",
            'content' => "required|string|max:255",
            'image_url' => "nullable|string|max:255"
        ];
    }

    public function updatingRules( int $id ): array
    {
        return $this->creatingRules();
    }

    public function onCreating( $model )
    {
        $model->user_id = $this->getUserId();
        $model->created_by = $this->getUserName();
        $model->status = 'PENDING';
    }

    /**
     * @param $model
     * @return void
     */
    public function onCreated( $model )
    {
        $email = removeSpecialChar($this->getUserName());
        $clientKey = app()->request->fcm_token;

        $fcm = new Firebase('AAAAylsewjA:APA91bEtL9Ago2PdnCGLQhbrrkczlEV1wIzSyvk3XXmY-3X0-oR6JyRVB9c-XBgB6QNDl-i_vUrDZUVK77UvP453wTZw8YNhzPKbvTO9AiI5mI0uFMrjvWZHuaxotbEzjVcWNG8iID8m');
        $user = $fcm->getUser($clientKey);
        if (!isset($user['rel']) || !count($user['rel']) || !isset($user['rel']['topics']) || !count($user['rel']['topics']) || !isset($user['rel']['topics'][$email])) {
            // need subscribe
            $fcm->subscribeTopic($clientKey, $email);
        }

        $fcm->push("/topics/$email", $model->title, $model->content, [
            'jenis' => $model->jenis,
            'navigate_to' => $model->navigate_to,
            'image_url' => $model->image_url
        ]);
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdating( $model )
    {
        $model->updated_by = $this->getUserName();
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdated( $model )
    {
        //
    }

    public function onRetrieved( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is creating or updating
     */
    public function onSaving( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is created or updated
     */
    public function onSaved( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is deleting
     */
    public function onDeleting( $model )
    {
        $model->deleted_by = $this->getUserName();
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is deleted
     */
    public function onDeleted( $model )
    {
        //
    }
}
