<?php

namespace App\Models;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ql_m_faskes_d_poli extends Model
{
    use HasFactory, ModelTrait;
    protected $table = 'ql_m_faskes_d_poli';
    protected $guarded = ['id','created_at','updated_at'];

    protected $joins = [];

    public $details = [];

    public function onCreating( $model )
    {
        $model->created_by = $this->getUserName();
        $model->status = 'ACTIVE';
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdating( $model )
    {
        $model->updated_by = $this->getUserName();
    }
}
