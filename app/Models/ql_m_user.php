<?php

namespace App\Models;

use App\Rules\MatchOldPassword;
use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class ql_m_user extends Model
{
    use HasFactory, ModelTrait, SoftDeletes;
    protected $table = 'ql_m_user';
    protected $guarded = ['id','created_at','updated_at'];

    protected $joins = [];

    public $details = [];

    public function creatingRules()
    {
        return [
            'name' => "nullable|string|max:100",
            'nik' => "nullable|string|unique:ql_m_user,nik",
            'name_prefix' => "nullable|string|max:50",
            'name_suffix' => "nullable|string|max:50",
            'gelar_prefix' => "nullable|string|max:50",
            'gelar_suffix' => "nullable|string|max:50",
            'instalasi_id' => "nullable|integer",
            'gender' => "nullable|string|max:30",
            'birth_place' => "nullable|string|max:30",
            'birth_date' => "nullable|date",
            'no_rm' => "nullable|string|max:100",
            'no_passport' => "nullable|string|max:100",
            'kewarganegaraan' => "nullable|string|max:30",
            'suku' => "nullable|string|max:100",
            'agama' => "nullable|string|max:100",
            'pendidikan' => "nullable|string|max:100",
            'pekerjaan' => "nullable|string|max:100",
            'status_pernikahan' => "nullable|string|max:50",
            'golongan_darah' => "nullable|string|max:2",
            'bahasa' => "nullable|string|max:100",
            'address' => "nullable|string|max:255",
            'address_rt' => "nullable|string|max:4",
            'address_rw' => "nullable|string|max:4",
            'address_provinsi' => "nullable|string|max:30",
            'address_kota' => "nullable|string|max:30",
            'address_kecamatan' => "nullable|string|max:30",
            'address_kelurahan' => "nullable|string|max:30",
            'address_kodepos' => "nullable|string|max:10"
        ];
    }

    public function updatingRules( int $id ): array
    {
        $rules = $this->creatingRules(); // menyamakan dengan create, tapi diubah sedikit
        foreach( $rules as $key => $value ){
            if( $key == 'nik' ){
                $rules[$key].=",$id"; //   untuk validasi unique selain diri sendiri
            }
        }
        return $rules;
    }

    public function onCreating( $model )
    {
        //
    }

    public function onCreated( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified
     */
    public function onUpdating( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when an existing model is modified.
     */
    public function onUpdated( $model )
    {
        $req = app()->request;
        if (isset($req->nik)) {
            $oauth_user = new oauth_user();
            $oauth_user->where('user_id', $this->autoDecrypt($model->id))
                ->update([
                    'nik' => $req->nik
                ]);
        }
    }

    public function onRetrieved( $model )
    {
        $model = Arr::except($model, ['password','email_verified_at']);
    }

    public function onDeleting($model)
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is created or updated - even if the model's attributes have not been changed
     */
    public function onSaving( $model )
    {
        //
    }

    /**
     * @param $model
     * @return void
     * events will dispatch when a model is created or updated - even if the model's attributes have not been changed
     */
    public function onSaved( $model )
    {
        $model = Arr::except($model, ['password','email_verified_at']);
    }

    public function customUpdate_password()
    {
        $request = app()->request;
        $input = $request->all();
        $validator = Validator::make($input, [
            'current_password' => ['required', new MatchOldPassword()],
            'new_password' => 'required',
            'c_new_password' => 'required|same:new_password'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', (array)$validator->errors()->getMessages());
        }

        DB::beginTransaction();
        try {
            ql_m_user::where('id', $this->getUserId())->update(['password' => bcrypt($request->new_password)]);
            oauth_user::where('id', $this->getUserId())->update(['password' => bcrypt($request->new_password)]);

            DB::commit();
            return $this->sendResponse(null, 'Password has been successfully to update!', 201);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->sendError('Update password has been failed.', [], 500);
        }
    }
}
