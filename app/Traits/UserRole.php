<?php

namespace App\Traits;

use App\Helpers\Cryptor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

trait UserRole
{
    /**
     * Get User Roles
     */
    public function getRoles( $id = null )
    {
        $userId = $id ?? (new Cryptor)->decrypt( Auth::user()->getAppUser()->getOriginal('id') );
        
        $data = DB::select("
            select 
            --menus.name, menus.modul, menus.sub_modul, menus.sequence, menus.endpoint, menus.url_path,
            accessd.create,accessd.read,accessd.show,accessd.delete,accessd.update,accessd.post,
                menus.modul as menu, menus.sub_modul as sub_menu, menus.name as child_menu, menus.url_path as xtype
            from web.set_mas_access_det_menu accessd
            join web.set_mas_menu menus on menus.id = accessd.menu_id 
            join web.set_mas_responsibility_det_access respd on respd.access_id = accessd.set_mas_access_id
            join web.set_mas_user_det_responsibility resp on resp.responsibility_id = respd.set_mas_responsibility_id 
            where menus.active_flag is true and resp.set_mas_user_id = $userId order by menus.sequence
        ");

        return $data;
    }

    public function getAppUser(){
        if( config('currentAppUser') ) return config('currentAppUser');
        $userId = Auth::user()->getOriginal('id');
        
        $data = DB::table('set_mas_user')
                ->select("id","active_flag")
                ->where('user_id',  $userId)
                ->first();

        config(['currentAppUser' => $data]);
        return $data;
    }

}
