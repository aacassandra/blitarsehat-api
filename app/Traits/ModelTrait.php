<?php

namespace App\Traits;

use App\Helpers\Cryptor;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

trait ModelTrait
{
    use ModelHelper, ModelScopes, ModelDetails;

    protected static function booted()
    {
        /**
         *
         * The retrieved event will dispatch when an existing model is retrieved from the database.
         * When a new model is saved for the first time, the creating and created events will dispatch.
         * The updating / updated events will dispatch when an existing model is modified and the save method is called.
         * The saving / saved events will dispatch when a model is created or updated
         *
         * https://laravel.com/docs/9.x/eloquent#events
         *
         */
        parent::boot();
        static::creating(function ($model) {
            if (method_exists($model, 'onCreating')) $model->onCreating($model);
        });

        self::created(function ($model) {
            if (method_exists($model, 'onCreated')) $model->onCreated($model);
        });

        self::saving(function ($model) {
            if (method_exists($model, 'onSaving')) $model->onSaving($model);
        });

        self::saved(function ($model) {
            if (method_exists($model, 'onSaved')) $model->onSaved($model);
        });

        self::updating(function ($model) {
            foreach ($model->getAttributes() as $key => $val) {
                if (!in_array($key, $model->getColumns($model->getTable()))) {
                    $model->offsetUnset($key);
                }

                // if ($val && !is_numeric($val)) {
                //     if ($key === 'id') {
                //         $model[$key] = (new Cryptor)->decrypt($val);
                //     } else if (Str::endswith($key, '_id') && $val && !is_numeric($val)) {
                //         $model[$key] = (new Cryptor)->decrypt($val);
                //     }
                // }
            }

            if (method_exists($model, 'onUpdating')) $model->onUpdating($model);
        });

        self::updated(function ($model) {
            if (method_exists($model, 'onUpdated')) $model->onUpdated($model);
        });

        self::deleting(function ($model) {
            if (method_exists($model, 'onDeleting')) {
                $model->onDeleting($model);
                $model->saveQuietly();
            };
        });

        self::deleted(function ($model) {
            if (method_exists($model, 'onDeleted')) $model->onDeleted($model);
        });

        self::retrieved(function ($model) {
            if (method_exists($model, 'onRetrieved')) {
                $model->onRetrieved($model);

                //  pas PUT/DELETE by id harus dikecualikan agar tidak diencrypt kolom2nya

                if( !app()->request->isMethod('GET') ) return;

                if (isset(app()->request->encrypt) && app()->request->encrypt === 'false') {
                    // abaikan
                } else {
                    foreach ($model->toArray() as $key => $val) {
                        if (Str::endsWith($key, '_id') && is_numeric($val)) {
                            $model[$key] = $model->encrypt($val);
                        } else if (strpos($key, '.') > -1) {
                            $exp = explode('.', $key);
                            if (Str::endsWith($exp[count($exp) - 1], '_id') && is_numeric($val)) {
                                $model[$key] = $model->encrypt($val);
                            } else if (Str::is('id', $exp[count($exp) - 1]) && is_numeric($val)) {
                                $model[$key] = $model->encrypt($val);
                            }
                        }
                    }
                }
            }
        });
    }

    //  Cara pakai: harus di dalam try catch, setelah validasi manual
    //  ->creatin();
    public function creatin($arrData = null, $customRules = [], $debug = false)
    {
        DB::beginTransaction();

        try {
            $reqArr = $arrData ?? app()->request->all();

            //  cast data joinan FK ke decrypted id
            if (isset($this->joins)) {
                foreach ($this->joins as $currentCol => $val) {
                    if (@$reqArr[$currentCol] && !is_numeric($reqArr[$currentCol])) {
                        $reqArr[$currentCol] = $this->decrypt($reqArr[$currentCol]) ?? $reqArr[$currentCol];
                    }
                }
            }

            foreach ($reqArr as $currentCol => $val) {
                if (Str::endswith($currentCol, '_id') && $val && !is_numeric($val)) {
                    $reqArr[$currentCol] = $this->decrypt($val) ?? $val;
                }
            }

            $rules = method_exists($this, 'creatingRules') ? $this->creatingRules() : [];
            $details = @$this->details ?? [];
            $fixedRules = [];
            if (count($customRules)) {
                $fixedRules = array_filter($customRules, function ($rule) {
                    return !is_array($rule);
                });
            } else {
                $fixedRules = array_filter($rules, function ($rule) {
                    return !is_array($rule);
                });
            }

            $validator = Validator::make($reqArr, $fixedRules);

            if ($validator->fails()) {
                config(['errors' => $validator->errors()->all()]);
                trigger_error("Maaf, data tidak valid.");
            }

            $columns = $this->getColumns($this->getTable());
            $active_flag_available = false;
            foreach ($columns as $col) {
                if ($col === 'active_flag') {
                    $active_flag_available = true;
                }
            }

            // pengecekan active_flag, jika tidak ada akan di set default = true
            if ($active_flag_available === true && !isset($reqArr['active_flag'])) {
                $reqArr['active_flag'] = true;
            }

            $createdModel = $this->create(Arr::only($reqArr, $columns));
            if (count($details)) {
                $this->creatinDetails($createdModel, $rules, $reqArr, $details);
            }

            foreach($createdModel->attributes as $key => $val){
                if(str_ends_with($key, "_id") && $val){
                    $createdModel[$key] = $this->encrypt($val);
                }
            }

            DB::commit();
            return response()->json([
                'message' => 'Successfully creating new data',
                'data' => $createdModel
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'errors' => @config('errors') ?? [$e->getMessage()],
                'errorDebug' => @config('errors') ?? [$e->getMessage() . "-" . $e->getLine() . "-" . $e->getFile()],
            ], 422);
        }
    }

    //  Cara pakai: harus di dalam try catch, setelah validasi manual
    //  ->updatin( id: 1 );
    public function updatin(int $id, $arrData = null, $customRules = [])
    {
        $createdModel = $this->find(
            $id
            /** Harus sudah didecrypt */
        );

        if (!$createdModel) {
            return response()->json([
                'message' => "Data Tidak Ditemukan untuk update",
                'errors' => ["No Data"]
            ], 404);
        }

        DB::beginTransaction();

        try {
            $reqArr = $arrData ?? app()->request->all();
            $rules = method_exists($this, 'updatingRules') ? $this->updatingRules($id) : [];
            $details = @$this->details ?? [];

            $fixedRules = [];
            if (count($customRules)) {
                $fixedRules = array_filter($customRules, function ($rule) {
                    return !is_array($rule);
                });
            } else {
                $fixedRules = array_filter($rules, function ($rule) {
                    return !is_array($rule);
                });
            }

            //  cast data joinan FK ke decrypted id
            if (isset($this->joins)) {
                foreach ($this->joins as $currentCol => $val) {
                    if (@$reqArr[$currentCol] && !is_numeric($reqArr[$currentCol])) {
                        $reqArr[$currentCol] = $this->decrypt($reqArr[$currentCol]) ?? $reqArr[$currentCol];
                    }
                }
            }

            foreach ($reqArr as $currentCol => $val) {
                if (Str::endswith($currentCol, '_id') && $val && !is_numeric($val)) {
                    $reqArr[$currentCol] = $this->decrypt($val) ?? $val;
                }
            }

            $validator = Validator::make($reqArr, $fixedRules);

            if ($validator->fails()) {
                config(['errors' => $validator->errors()->all()]);
                trigger_error("Maaf, data tidak valid.");
            }

            $columns = $this->getColumns($this->getTable());
            // $reqArr = Arr::except($reqArr, ['id']);
            $createdModel->update(Arr::only($reqArr, $columns));

            if (count($details)) {
                $this->updatinDetails($createdModel, $rules, $reqArr, $details, $id, null, true);
            }

            foreach($createdModel->attributes as $key => $val){
                if ($key === 'id') {
                    // $createdModel[$key] = $this->encrypt($val); // ini tidak perlu mas afif yg ganteng, sudah encryptan
                } else if(str_ends_with($key, "_id") && $val){
                    $createdModel[$key] = $this->encrypt($val);
                }
            }

            DB::commit();
            return response()->json([
                'message' => 'Successfully updating data',
                'data'  => $createdModel
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'errors' => @config('errors') ?? [$e->getMessage() . "-" . $e->getLine() . "-" . $e->getFile()]
            ], 422);
        }
    }

    //  Cara pakai: harus di dalam try catch
    //  ->deletin( id: 1, softDelete: true );
    public function deletin(int $id, bool $softDelete = true)
    {
        DB::beginTransaction();
        try {
            if (isset($this->children)) {
                foreach ($this->children as $child => $column) {
                    $childClass = new $child;
                    if ($childClass->where($column, $id)->exists()) {
                        trigger_error("Resource is still referenced in " . $childClass->getTable());
                    }
                }
            }

            $m = $this->getModel($this->getOnlyTable());
            $items = $this->withoutEvents(function () use ($m, $id) {
                return $m->where($m->getKeyName(), $id)->get();
            });

            if (count($items)) {
                foreach ($items as $item) {
                    if ($softDelete) {
                        // Untuk soft deleting
                        $item->delete();
                    } else {
                        // Untuk force deleting
                        $item->forceDelete();
                    }
                }

                $details = @$this->details ?? [];
                $this->deletinDetails($softDelete, $m, $details, $id, true);
            } else {
                return response()->json([
                    'message' => 'Data not found'
                ], 404);
            }

            DB::commit();
            return response()->json([
                'message' => 'Successfully deleting data'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message' => 'Maaf terjadi kesalahan',
                'errors' => [$e->getMessage()]
            ], 422);
        }
    }
}
