<?php

namespace App\Traits;

use App\Models\ApiModel;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Helpers\Cryptor;

trait ModelHelper
{
    public $useEncryption  = true;

    public function encrypt(string $plainText): mixed
    {
        return (new Cryptor)->encrypt($plainText);
    }

    public function decrypt(string $encryptedText): mixed
    {
        return (new Cryptor)->decrypt($encryptedText);
    }

    private function getColumns($table)
    {
        $columns = config("columns_$table");    // ambil dari config jika ada agar tidak berkali2 getColumnListing
        if (!$columns) {
            $db = DB::connection()->getPdo();
            $rs = $db->query("SELECT * FROM $table LIMIT 0");
            for ($i = 0; $i < $rs->columnCount(); $i++) {
                $col = $rs->getColumnMeta($i);
                $columns[] = $col['name'];
            }

            // tidak support untuk multischema
            // $columns = Schema::getColumnListing('set_mas_gen');
            config(["columns_$table" => $columns]);
        }
        return $columns;
    }

    public function getIdAttribute(mixed $val)
    {

        if (app()->request->has('encrypt') && Str::lower(app()->request->encrypt) == "false") {
            return $val;
        }

        if (!$this->useEncryption) {
            return (int)$val;
        }

        return $this->encrypt($val);
    }

    private function getSchema(string $modelOrTable)
    {
        $schemas = config('model-trait.schema');

        // Pengecekan schema
        $match = false;
        $selectedSchema = "";
        foreach ($schemas as $schema => $tables) {
            foreach ($tables as $tbl) {
                if ($tbl === $modelOrTable) {
                    $match = true;
                    $selectedSchema = $schema;
                }
            }
        }

        // Gunakan default schema jika table/model tidak di daftarkan ke konfigurasi schema
        // konfigurasi ada di file ./config/model-trait.php
        if (!$match) {
            $selectedSchema = config('model-trait.default');
        }

        return $selectedSchema;
    }

    private function getModel(string $modelOrTable)
    {
        $selectedSchema = $this->getSchema($modelOrTable);

        if (class_exists("App\Models\\$modelOrTable")) {
            $className = "App\Models\\$modelOrTable";
            $model = new $className;
            if ($model->has_custom()) {
                // Jika membuat model custom
                // pada model tambahkan
                // protected is_custom = true;
                return $model->setTable("$selectedSchema.{$model->getTable()}");
            } else {
                return $model->setTable("$selectedSchema.$modelOrTable");
            }
        } elseif (Schema::hasTable($modelOrTable)) {
            return $this->makeModel( $selectedSchema, $modelOrTable );
        }

        return false;
    }

    private function makeModel(string $selectedSchema, string $tableName)
    {
        $model = new ApiModel;
        $model->setTable("$selectedSchema.$tableName");
        return $model;
    }

    /**
     * @override Laravel function
     */
    public function getCasts()
    {
        $casts = $this->casts;
        return array_merge([
            'created_at' => 'date:d/m/Y H:i',
            'updated_at' => 'date:d/m/Y H:i'
        ], $casts);
    }

    public function getDeletedAtAttribute(mixed $value)
    {
        return $value ? Carbon::create($value)->format('d/m/Y H:i') : $value;
    }

    public function arr2json($arr)
    {
        return json_decode(json_encode($arr));
    }

    public function json2arr($json)
    {
        return json_decode(json_encode($json), true);
    }

    // Untuk manual validation di controller menggunakan rules yang ada di model
    public function getValidation(string $get, string $method)
    {
        $reqArr = app()->request->all();
        $rules = [];

        if (method_exists($this, $method)) {
            if ($method === 'create') {
                $rules = $this->creatingRules();
            } elseif ($method === 'update') {
                $rules = $this->updatingRules();
            } elseif ($method === 'delete') {
                $rules = $this->deletingRules();
            }
        }

        $details = @$this->details ?? [];
        $fixedRules = array_filter($rules, function ($rule) {
            return !is_array($rule);
        });

        foreach ($reqArr as $currentCol => $val) {
            if (Str::endswith($currentCol, '_id') && $val && !is_numeric($val)) {
                $reqArr[$currentCol] = $this->autoDecrypt($val) ?? $val;
            }
        }

        $validator = Validator::make($reqArr, $fixedRules);

        if ($get === 'status') {
            if ($validator->fails()) {
                return 'error';
            } else {
                foreach ($reqArr as $key => $val) {
                    if (method_exists($this, $key) && is_array($val)) {
                        //  jika pakai hasMany
                    } elseif (in_array($key, $details)) {
                        foreach ($val as $dtlIdx => $dtlRow) {
                            if (isset($rules[$key . ".*"])) {

                                foreach ($dtlRow as $currentCol => $val) {
                                    if (Str::endswith($currentCol, '_id') && $val && !is_numeric($val)) {
                                        $dtlRow[$currentCol] = $this->autoDecrypt($val) ?? $val;
                                    }
                                }

                                $dtlValidator = Validator::make($dtlRow, $rules[$key . ".*"]);

                                if ($dtlValidator->fails()) {
                                    return 'error';
                                }
                            }
                        }
                    }
                }
            }
        } elseif ($get === 'output') {
            if ($validator->fails()) {
                return $this->arr2json([
                    "message" => "Maaf, data tidak valid.",
                    "errors" => $validator->errors()->all()
                ]);
            } else {
                foreach ($reqArr as $key => $val) {
                    if (method_exists($this, $key) && is_array($val)) {
                        //  jika pakai hasMany
                    } elseif (in_array($key, $details)) {
                        foreach ($val as $dtlIdx => $dtlRow) {
                            if (isset($rules[$key . ".*"])) {

                                foreach ($dtlRow as $currentCol => $val) {
                                    if (Str::endswith($currentCol, '_id') && $val && !is_numeric($val)) {
                                        $dtlRow[$currentCol] = $this->autoDecrypt($val) ?? $val;
                                    }
                                }

                                $dtlValidator = Validator::make($dtlRow, $rules[$key . ".*"]);

                                if ($dtlValidator->fails()) {
                                    return $this->arr2json([
                                        "message" => "Maaf, data tidak valid.",
                                        "errors" => $dtlValidator->errors()->all()
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function getUserId()
    {
        if (app()->request->user('api') !== null) {
            return app()->request->user('api')->id;
        } else {
            return null;
        }
    }

    public function getUserName()
    {
        if (app()->request->user('api') !== null) {
            return app()->request->user('api')->username;
        } else {
            return null;
        }
    }

    public function route( string $method ) {
        $match = false;
        $except = $this->except ?? [];
        foreach ($except as $ex) {
            if ($ex === $method) {
                $match = true;
            }
        }
        if ($match) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Melakukan listing available scope di model saat get data
     */
    public function getAvailableScopes()
    {
        $model = $this;
        $scopes = array_filter(get_class_methods($model), function ($dt) use ($model) {
            $dt = Str::lower($dt);
            return Str::startsWith($dt, 'scope') && !in_array(Str::replace('scope', '', $dt), $model->scopes);
        });

        return array_map(function ($dt) {
            return lcfirst(Str::replace('scope', '', $dt));
        }, array_values($scopes));
    }

    /**
     * Melakukan listing available scope di model saat get data
     */
    public function getAvailableMethods()
    {
        $model = $this;
        $methods = array_filter(get_class_methods($model), function ($dt) {
            $dt = Str::lower($dt);
            return Str::startsWith($dt, ['custom','getfunc']);
        });

        return array_map(function ($dt) {
            if(Str::startsWith($dt, 'custom')){
                return lcfirst(Str::replace('custom', '', $dt))." :POST";
            }else if(Str::startsWith($dt, 'getfunc')){
                return lcfirst(Str::replace('getfunc', '', $dt))." :GET";
            }
        }, array_values($methods));
    }

    /**
     * Melakukan generesasi number otomatis
     *
     * @param $length
     * @return string
     */
    public function getRandomNumber($length) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    /**
     * Untuk get table dan schema
     * Ini juga dianjurkan untuk pemakaian di file migrations
     *
     * @param string $table
     * @return string
     */
    public function getTableWithSchema(string $table) {
        $schm = $this->getSchema($table);
        return "$schm.$table";
    }

    public function autoDecrypt($val)
    {
        return $val === null ? null : (is_numeric($val) && isset(app()->request->encrypt) && app()->request->encrypt === 'false' ?
            $val :
            (is_numeric($val) ? $val : $this->decrypt($val)));
    }

    public function has_custom()
    {
        return $this->is_custom ?? false;
    }

    public function getOnlyTable()
    {
        $b = $this->getTable();
        $schemas = config('model-trait.schema');
        $tblName = $b;

        // Pengecekan schema
        foreach ($schemas as $schema => $tables) {
            if (str_contains($b, "$schema.")) {
                $tblName = str_replace("$schema.","",$b);
            }
        }

        return $tblName;
    }

    public function getScopes()
    {
        $relatedScope = ['joinin','filterin','selectin'];
        return $relatedScope;
    }

    public function getFixRules($rules)
    {
        return array_filter($rules, function ($rule) {
            return !is_array($rule);
        });
    }

    /**
     * Validator include
     * 1. auto check untuk field yang perlu di decrypt
     * 2. penghapusan rules yang ada detailnya
     * @param array $data
     * @param array $rules
     * @param bool $debug
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data, array $rules, $debug = false)
    {
        foreach ($data as $currentCol => $value) {
            if ($currentCol === 'id' && $value && !is_numeric($value)) {
                $data[$currentCol] = $this->decrypt($value) ?? $value;
            } else if (Str::endswith($currentCol, '_id') && $value && !is_numeric($value)) {
                $data[$currentCol] = $this->decrypt($value) ?? $value;
            }
        }
        if ($debug === true) {
            // masukan variable apa yang ingi kalian debug
            // ...
        }
        return Validator::make($data, $this->getFixRules($rules));
    }

    /**
     * Mengurusi auto encrypt untuk semua field dalam object/array
     * @param $data
     * @param $inArray
     * @return mixed
     */
    public function encryptAllFields($data, $inArray = false)
    {
        if (isset(app()->request->encrypt) && app()->request->encrypt === 'false') {
            //
        } else {
            if ($inArray === true) {
                foreach ($data as $index => $dt)
                {
                    foreach ($dt as $key => $val) {
                        if ($key === 'id') {
                            $dt[$key] = $this->encrypt($val);
                        } else if (Str::endsWith($key, '_id') && is_numeric($val)) {
                            $dt[$key] = $this->encrypt($val);
                        } else if (strpos($key, '.') > -1) {
                            $exp = explode('.', $key);
                            if (Str::endsWith($exp[count($exp) - 1], '_id') && is_numeric($val)) {
                                $dt[$key] = $this->encrypt($val);
                            } else if (Str::is('id', $exp[count($exp) - 1]) && is_numeric($val)) {
                                $dt[$key] = $this->encrypt($val);
                            }
                        }
                    }
                    $data[$index] = $dt;
                }
            } else {
                foreach ($data as $key => $val) {
                    if ($key === 'id') {
                        $data[$key] = $this->encrypt($val);
                    } else if (Str::endsWith($key, '_id') && is_numeric($val)) {
                        $data[$key] = $this->encrypt($val);
                    } else if (strpos($key, '.') > -1) {
                        $exp = explode('.', $key);
                        if (Str::endsWith($exp[count($exp) - 1], '_id') && is_numeric($val)) {
                            $data[$key] = $this->encrypt($val);
                        } else if (Str::is('id', $exp[count($exp) - 1]) && is_numeric($val)) {
                            $data[$key] = $this->encrypt($val);
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Mengurusi auto decrypt untuk semua field dalam object/array
     * @param $data
     * @param $inArray
     * @return mixed
     */
    public function decryptAllFields($data, $inArray = false)
    {
        if ($inArray === true) {
            foreach ($data as $index => $dt) {
                $tempData = $this->arr2json($dt);
                if (is_string($tempData)) {
                    $data[$index] = $this->autoDecrypt($tempData) ?? $tempData;
                } else if (is_integer($tempData)) {
                    $data[$index] = $tempData;
                } else {
                    foreach ($tempData as $currentCol => $value) {
                        if ($currentCol === 'id' && $value && !is_numeric($value)) {
                            $data[$index][$currentCol] = $this->autoDecrypt($value) ?? $value;
                        } else if (Str::endswith($currentCol, '_id') && $value && !is_numeric($value)) {
                            $data[$index][$currentCol] = $this->autoDecrypt($value) ?? $value;
                        }
                    }
                }
            }
        } else {
            $tempData = $this->arr2json($data);
            foreach ($tempData as $currentCol => $value) {
                if ($currentCol === 'id' && $value && !is_numeric($value)) {
                    $data[$currentCol] = $this->autoDecrypt($value) ?? $value;
                } else if (Str::endswith($currentCol, '_id') && $value && !is_numeric($value)) {
                    $data[$currentCol] = $this->autoDecrypt($value) ?? $value;
                }
            }
        }

        return $data;
    }

    public function onGenerateRandomString($prefix = '', $length = 10, $hasNumber = true, $hasLowercase = true, $hasUppercase = true): string
    {
        $string = '';
        if ($hasNumber)
            $string .= '0123456789';
        if ($hasLowercase)
            $string .= 'abcdefghijklmnopqrstuvwxyz';
        if ($hasUppercase)
            $string .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return $prefix . substr(str_shuffle(str_repeat($x = $string, ceil($length / strlen($x)))), 1, $length);
    }

    /**
     * success response method.
     * @param $result
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($result, string $message = '', int $code = 200)
    {
        $response = [
            'success' => true
        ];

        if ($result) {
            $response['data'] = $result;
        }

        if ($message) {
            $response['message'] = $message;
        }

        return response()->json($response, $code);
    }

    /**
     * return error response.
     * @param $errorMessage
     * @param array $errors
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($errorMessage, array $errors = [], int $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $errorMessage,
        ];


        if(!empty($errors)){
            $response['errors'] = $errors;
        }

        return response()->json($response, $code);
    }
}
