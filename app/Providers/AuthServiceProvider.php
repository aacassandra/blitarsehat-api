<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // https://laravel.com/docs/9.x/passport#token-lifetimes
        Passport::tokensExpireIn(now()->addDays(env('PASSPORT_TOKEN_EXPIRE_IN', 15)));
        Passport::refreshTokensExpireIn(now()->addDays(env('PASSPORT_REFRESH_TOKEN_EXPIRE_IN', 30)));
        Passport::personalAccessTokensExpireIn(now()->addMonths(env('PASSPORT_PERSONAL_TOKEN_IN', 6)));
    }
}
