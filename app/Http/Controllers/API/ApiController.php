<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Models\ApiModel;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class ApiController extends BaseController
{
    private function makeModel( string $selectedSchema, string $tableName )
    {
        $model = new ApiModel;
        $model->setTable("$selectedSchema.$tableName");
        return $model;
    }

    private function getModel( string $modelOrTable ){
        $schemas = config('model-trait.schema');

        // Pengecekan schema
        $match = false;
        $selectedSchema = "";
        foreach ($schemas as $schema => $tables) {
            foreach ($tables as $tbl) {
                if ($tbl === $modelOrTable) {
                    $match = true;
                    $selectedSchema = $schema;
                }
            }
        }

        // Gunakan default schema jika table/model tidak di daftarkan ke konfigurasi schema
        // konfigurasi ada di file ./config/model-trait.php
        if (!$match) {
            $selectedSchema = config('model-trait.default');
        }

        if( class_exists("App\Models\\$modelOrTable") ){
            $className = "App\Models\\$modelOrTable";
            $model = new $className;


            if ($model->has_custom()) {
                // Jika membuat model custom
                // pada model tambahkan
                // protected is_custom = true;
                return $model->setTable("$selectedSchema.{$model->getTable()}");
            } else {
                return $model->setTable("$selectedSchema.$modelOrTable");
            }
        }elseif( Schema::hasTable( $modelOrTable ) ){
            return $this->makeModel( $selectedSchema, $modelOrTable );
        }

        return false;
    }

    private function generateApiDoc(){
        $tables = Schema::getAllTables();
        $prefix = "api/v1/";
        $tables = array_filter($tables, function($table){
            return
                !Str::contains( $table->tablename, "telescope") &&
                !Str::contains( $table->tablename, "oauth") &&
                !Str::contains( $table->tablename, "jobs") &&
                !Str::contains( $table->tablename, "permissions") &&
                !Str::contains( $table->tablename, "token");
        });
        $fixedTables = array_map(function($table)use($prefix){
            return [
                'GET' =>    $prefix.$table->tablename,
                'GET_ID' => $prefix.$table->tablename."/{ID}",
                'POST' =>   $prefix.$table->tablename,
                'DELETE' => $prefix.$table->tablename."/{ID}",
                'PUT/PATCH' =>  $prefix.$table->tablename."/{ID}",
                'SCHEMA' => "additional/structure/$table->tablename"
            ];
        }, $tables);
        return $fixedTables;
    }

    public function index(
        Request $request,
        string $parent = null,
        string $parent_id = null,
        string $detail = null,
        string $detail_id = null,
        string $subdetail = null,
        string $subdetail_id = null
    ){
        // ======================== API Documentation
        if( !$parent ){
            $metaData = [
                "params" => [
                    "paginate"=>"numeric, ex: 25 [Untuk melakukan paginasi data]",
                    "where"=>"text, ex: name = 'FAJAR' [Melakukan custom where raw query]",
                    "wherein"=>"text, ex: id = [1,2] [Melakukan custom where in query]",
                    "wherenotin"=>"text, ex: id = [1,2] [Melakukan custom where not in query]",
                    "distinct"=>"text, ex: group [Mendapatkan nilai group dari suatu kolom]",
                    "search" => "text, ex: jawatimur [Melakukan searching ke all columns dengan logika LIKE]",
                    "selectfield"=> "text, ex: kolom1,kolom_N [Memilih kolom yang diinginkan saja, bisa subquery AS]",
                    "addselect"=> "text, ex: kolom1,kolom_N [Menambahkan kolom tambahan, bisa subquery AS]",
                    "data_filter_KOLOM" => "text [Memfilter data dengan logika MATCH]",
                    "join" => "boolean, ex: false [Meng-enable auto join atau tidak]",
                    "joins" => "string, ex: user_id=users atau user_id=users.kolomnya [Men-join-kan ngefly]",
                    "orderby"=>"string, ex: this.id [Untuk melakukan order by id table bersangkutan]",
                    "ordertype"=>"string, ex: DESC [Untuk melakukan order type ASC atau DESC]",
                    "encrypt"=>"string, ex: false [Untuk mematikan encryption data kolom ID]",
                    "all"=>"string, ex: true [Get data termasuk yang terhapus(deleted)]",
                ],
                "endpoints" => $this->generateApiDoc()
            ];

            return view('apidoc', compact('metaData'));
        }

        // ======================== GET MODEL / FLY MODEL BY TABLE
        $parentModel = $this->getModel( $parent );

        if( !$parentModel ){
            return response()->json([
                'message' => "Resource $parent tidak ditemukan",
                "errors" => ["Resource $parent tidak ditemukan"]
            ], 404);
        }

        // ======================== API /parent
        if( !$parent_id ){
            if( $request->isMethod('GET')){
                //  INDEX
                // Pengecekan Except Route
                if ($parentModel->route('index')) {
                    return response()->json([
                        'message' => "Resource $parent tidak ditemukan",
                        'errors' => ["Resource $parent tidak ditemukan"]
                    ], 404);
                }

                if($request->has('all')){
                    $parentModel = $parentModel->withTrashed();
                }

                $availableScopes = $parentModel->getAvailableScopes();
                $availableMethods = $parentModel->getAvailableMethods();
                if( app()->request->has('distinct') ){
                    $data = [
                        'data' => $parentModel->distinct()->pluck(app()->request->distinct)->all()
                    ];
                } else {
                    $clientScopes = $request->has('scopes')? explode( ',', $request->scopes ):[];
                    foreach($clientScopes as $scope){
                        if(!in_array($scope, $availableScopes)){
                            return response()->json([
                                'message' => "failed to retrieve data",
                                'errors' => [ "scope $scope tidak tersedia untuk resource ini" ]
                            ], 404);
                        }
                    }
                    $data = $parentModel
                        ->scopes( array_merge($parentModel->scopes, $clientScopes ))
                        ->paginate( $request->paginate ?? 25 );

                    //if (method_exists($parentModel, '__construct') && method_exists($parentModel, 'onRetrieved')) {
                    //    foreach ($data as $dt)
                    //    {
                    //        $parentModel->onRetrieved( $dt );
                    //        if (isset(app()->request->encrypt) && app()->request->encrypt === 'false') {
                    //            // abaikan
                    //        } else {
                    //            foreach ($dt->toArray() as $key => $val) {
                    //                if (Str::endsWith($key, '_id') && is_numeric($val)) {
                    //                    $dt[$key] = $dt->encrypt($val);
                    //                } else if (strpos($key, '.') > -1) {
                    //                    $exp = explode('.', $key);
                    //                    if (Str::endsWith($exp[count($exp) - 1], '_id') && is_numeric($val)) {
                    //                        $dt[$key] = $dt->encrypt($val);
                    //                    } else if (Str::is('id', $exp[count($exp) - 1]) && is_numeric($val)) {
                    //                        $dt[$key] = $dt->encrypt($val);
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                }

                return collect([
                    'success' => true,
                    'available_scopes' => $availableScopes,
                    'available_methods' => $availableMethods
                ])->merge($data);
            }elseif( $request->isMethod('POST')){
                // STORE
                // Pengecekan Except Route
                if ($parentModel->route('store')) {
                    return response()->json([
                        'success' => false,
                        'message' => "Resource $parent tidak ditemukan",
                        'errors' => ["Resource $parent tidak ditemukan"]
                    ], 404);
                }

                return $parentModel->creatin();
            }

            // ======================== API /parent/parent_id
        }elseif( $parent_id ){
            //  customFunction atau getfuncFunction
            if( $request->isMethod('GET') && ( method_exists( $parentModel, "custom$parent_id" ) || method_exists($parentModel, 'getfunc'.ucfirst( $parent_id ) ) ) ){
                $function = method_exists( $parentModel, "custom$parent_id" ) ? "custom$parent_id" : 'getfunc'.ucfirst( $parent_id );
                return $parentModel->$function( $request , $detail);
            }
            else if(!$detail){
                $decryptedId = is_numeric($parent_id) && isset($request->encrypt) && $request->encrypt === 'false' ? $parent_id : $parentModel->decrypt( $parent_id );
                if( $request->isMethod('GET')){
                    //  SHOW
                    // Pengecekan Except Route
                    if ($parentModel->route('show')) {
                        return response()->json([
                            'message' => "Resource $parent tidak ditemukan",
                            'errors' => ["Resource $parent tidak ditemukan"]
                        ], 404);
                    }

                    $availableScopesForById = array_filter($parentModel->scopes, function($scope){
                        return !in_array( $scope, [
                            'wherin','whereadnotin','whereadin','distinctin','orderin','filterin','searchin'
                        ]) ;
                    });
                    $data = $parentModel->scopes( $availableScopesForById );
                    if($request->has('all')){
                        $data = $data->withTrashed();
                    }
                    $data = $data->find( $decryptedId );

                    if( $data ){
                        $data = $data->toArray();
                    }else{
                        return response()->json(['message'=>'Data tidak ditemukan','errors'=>['No Data']],404);
                    }

                    if( @$parentModel->details ){
                        $data = $parentModel->detailinDetails($parentModel->details, $parent, $decryptedId, $data);
                        if(isset($data->original)){
                            return response()->json($data->original, $data->status());
                        }
                    }

                    if( !$data ){
                        return response()->json([
                            'message'   => "No data was found",
                            'data'      => null
                        ], 404);
                    }
                    return [
                        'data' => $data
                    ];
                }elseif( $request->isMethod('PUT')){
                    //  UPDATE
                    // Pengecekan Except Route
                    if ($parentModel->route('update')) {
                        return response()->json([
                            'message' => "Resource $parent tidak ditemukan",
                            'errors' => ["Resource $parent tidak ditemukan"]
                        ], 404);
                    }

                    $decryptedId = is_numeric($parent_id) && isset($request->encrypt) && $request->encrypt === 'false' ? $parent_id : $parentModel->decrypt( $parent_id );
                    return $parentModel->updatin( $decryptedId );
                }elseif( $request->isMethod('DELETE')){
                    // DESTROY
                    // Pengecekan Except Route
                    if ($parentModel->route('destroy')) {
                        return response()->json([
                            'message' => "Resource $parent tidak ditemukan",
                            'errors' => ["Resource $parent tidak ditemukan"]
                        ], 404);
                    }

                    $decryptedId = $parentModel->decrypt( $parent_id );
                    return $parentModel->deletin( $decryptedId );
                }elseif( $request->isMethod('POST') && method_exists($parentModel, 'custom'.ucfirst( $parent_id ) ) ){
                    //  Custom Function

                    // Pengecekan Except Route
                    if ($parentModel->route('custom')) {
                        return response()->json([
                            'message' => "Resource $parent tidak ditemukan",
                            'errors' => ["Resource $parent tidak ditemukan"]
                        ], 404);
                    }

                    $function = 'custom'.ucfirst( $parent_id );
                    return $parentModel->$function( $request );
                }
            }
        }
    }
    public function test(){
        return true;
    }
}
