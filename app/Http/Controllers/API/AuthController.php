<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\ql_m_user;
use App\Models;
use App\Models\oauth_user;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendOtpCode;

class AuthController extends BaseController
{
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->identity, 'password' => $request->password])){
            $user = Auth::user();
            $success['role'] = $user->role;
            $success['nik'] = $user->nik;
            $success['email'] = $user->email;
            $success['name'] = $user->name;
            $success['username'] = $user->username;
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['token_type'] = 'Bearer';

            return $this->sendResponse($success, 'User login successfully with email.');
        } else if (Auth::attempt(['username' => $request->identity, 'password' => $request->password])) {
            $user = Auth::user();
            $success['role'] = $user->role;
            $success['nik'] = $user->nik;
            $success['email'] = $user->email;
            $success['name'] = $user->name;
            $success['username'] = $user->username;
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['token_type'] = 'Bearer';

            return $this->sendResponse($success, 'User login successfully with username.');
        } else if (Auth::attempt(['nik' => $request->identity, 'password' => $request->password])) {
            $user = Auth::user();
            $success['role'] = $user->role;
            $success['nik'] = $user->nik;
            $success['email'] = $user->email;
            $success['name'] = $user->name;
            $success['username'] = $user->username;
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['token_type'] = 'Bearer';

            return $this->sendResponse($success, 'User login successfully with username.');
        } else {
            $user = DB::table('ql_m_user')
                ->orWhere('email', $request->identity)
                ->orWhere('nik', $request->identity)
                ->where('status', 'PENDING');

            if ($user->exists()) {
                return $this->sendError('User is not verified.', [], 403);
            } else {
                return $this->sendError('Invalid credentials.', [], 401);
            }
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nik' => 'required|string|unique:ql_m_user,nik',
            'name' => 'required|string',
            'email' => "required|email|unique:ql_m_user,email",
            'phone' => "required|string|unique:ql_m_user,phone",
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors()->all(), 400);
        }

        DB::beginTransaction();
        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $input['status'] = 'PENDING';
            $input['name'] = $request->name;
            $input['username'] = $request->email;
            $input['created_at'] = now();
            $input = Arr::except($input, ['c_password']);
            $user = DB::table('ql_m_user')->insertGetId($input);
            $user = json2arr(DB::table('ql_m_user')->find($user));
            $otp_code = random_int(100000, 999999);
            Cache::put("otp_code_$otp_code", true, 86400);
            Cache::put("otp_code_{$otp_code}_data", $user['id'], 86400);
            Cache::put("otp_code_{$otp_code}_for", 'signup', 86400);
            $data = [
                'otp_code' => $otp_code,
                'mode' => 'register'
            ];

            Mail::to($request->email)->send(new SendOtpCode($data));
            DB::commit();
            return $this->sendResponse(
                Arr::only($user, ['nik','email','phone','username','created_at','name']), 'Register successfully. Please check your email to get OTP Code and will be expired after 24 hours.'
            );
        } catch (Exception $e) {
            DB::rollBack();
            return $this->sendError('Register failed.', [], 500);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return $this->sendResponse(null, 'User has been successfully to logout');
    }

    public function me(Request $req = null)
    {
        $data = $req ? $req->user() : app()->request->user();
        $users = new Models\users();
        $data = $users->arr2json($data);
        $data->id = app()->request->encrypt === 'false' ? $data->id : $users->encrypt($data->id);
        $data->user_id = app()->request->encrypt === 'false' ? $data->user_id : $users->encrypt($data->user_id);

        return $this->sendResponse($data);
    }

    public function forgot_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'identity' => 'required|string'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors()->all(), 400);
        }

        $user = DB::table('oauth_user')->where('nik', $request->identity)->orWhere('email', $request->email)->orWhere('username', $request->identity);
        if ($user->exists()) {
            $user = $user->first();
            $otp_code = random_int(100000, 999999);
            Cache::put("otp_code_$otp_code", true, 86400);
            Cache::put("otp_code_{$otp_code}_data", $user->id, 86400);
            Cache::put("otp_code_{$otp_code}_for", 'forgot_password', 86400);
            $data = [
                'otp_code' => $otp_code,
                'mode' => 'forgot-password'
            ];
            Mail::to($user->email)->send(new SendOtpCode($data));
            return $this->sendResponse(
                null, 'Please check your email to get OTP Code and will be expired after 24 hours.'
            );
        } else {
            return $this->sendError('Invalid credentials.', [], 401);
        }
    }

    public function signup_verification(Request $request, $otp_code)
    {
        DB::beginTransaction();
        try {
            if (Cache::has("otp_code_$otp_code")) {
                $for = Cache::get("otp_code_{$otp_code}_for");
                if ($for === 'signup') {
                    $userId = Cache::get("otp_code_{$otp_code}_data");
                    $user = DB::table('ql_m_user')->find($userId);

                    DB::table('oauth_user')->insert([
                        "nik" => $user->nik,
                        "name" => $user->name,
                        "email" => $user->email,
                        "phone" => $user->phone,
                        "role" => $user->role,
                        "password" => $user->password,
                        "username" => $user->email,
                        "user_id" => $userId,
                        "status" => "ACTIVE",
                        'created_at' => now()
                    ]);

                    DB::table('ql_m_user')->where('id', $userId)->update([
                        'status' => 'REGISTERED',
                        'email_verified_at' => now(),
                        'updated_at' => now()
                    ]);

                    Cache::put("otp_code_$otp_code", null);
                    Cache::put("otp_code_{$otp_code}_data", null);
                    DB::commit();
                    return $this->sendResponse(null, 'Email has been sucessfully to verified');
                } else {
                    DB::rollBack();
                    return $this->sendError('OTP Code is invalid', [], 400);
                }
            } else {
                DB::rollBack();
                return $this->sendError('OTP Code is invalid', [], 400);
            }
        } catch (Exception $e) {
            DB::rollBack();
            return $this->sendError('Register failed.', null, 500);
        }
    }

    public function forgot_password_verification(Request $request, $otp_code)
    {
        $validator = Validator::make($request->all(), [
            'identity' => 'required|string',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors()->all(), 400);
        }

        if (Cache::has("otp_code_$otp_code")) {
            $for = Cache::get("otp_code_{$otp_code}_for");
            if ($for === 'forgot_password') {
                $user = DB::table('oauth_user')->where('nik', $request->identity)->orWhere('email', $request->email)->orWhere('username', $request->identity);
                if ($user->exists()) {
                    $user = $user->first();
                    DB::beginTransaction();
                    try {
                        ql_m_user::where('id', $user->user_id)->update(['password' => bcrypt($request->password)]);
                        oauth_user::where('id', $user->id)->update(['password' => bcrypt($request->password)]);

                        Cache::put("otp_code_$otp_code", null);
                        Cache::put("otp_code_{$otp_code}_data", null);
                        DB::commit();
                        return $this->sendResponse(null, 'Password has been successfully to update!', 201);
                    } catch (Exception $e) {
                        DB::rollBack();
                        return $this->sendError('Update password has been failed.', [], 500);
                    }
                } else {
                    return $this->sendError('Invalid credentials.', [], 401);
                }
            } else {
                DB::rollBack();
                return $this->sendError('OTP Code is invalid', [], 400);
            }
        } else {
            return $this->sendError('OTP Code is invalid', [], 400);
        }
    }

    public function signup_resend_otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'identity' => 'required|string'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors()->all(), 400);
        }

        $user = DB::table('ql_m_user')
            ->orWhere('nik', $request->identity)
            ->orWhere('email', $request->identity)
            ->orWhere('username', $request->identity)->first();
        if ($user) {
            if ($user->status === 'PENDING') {
                $user = $user->first();
                $otp_code = random_int(100000, 999999);
                Cache::put("otp_code_$otp_code", true, 86400);
                Cache::put("otp_code_{$otp_code}_data", $user->id, 86400);
                Cache::put("otp_code_{$otp_code}_for", 'signup', 86400);
                $data = [
                    'otp_code' => $otp_code,
                    'mode' => 'register'
                ];
                Mail::to($user->email)->send(new SendOtpCode($data));
                return $this->sendResponse(
                    null, 'Please check your email to get OTP Code and will be expired after 24 hours.'
                );
            } else {
                return $this->sendError('The user is already registered and there is no need to re-send the OTP Code', [], 400);
            }
        } else {
            return $this->sendError('Invalid credentials.', [], 401);
        }
    }
}
