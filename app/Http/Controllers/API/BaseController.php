<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller as Controller;


class BaseController extends Controller
{
    /**
     * success response method.
     * @param $result
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($result, string $message = '', int $code = 200)
    {
        $response = [
            'success' => true
        ];

        if ($result) {
            $response['data'] = $result;
        }

        if ($message) {
            $response['message'] = $message;
        }

        return response()->json($response, $code);
    }

    /**
     * return error response.
     * @param $errorMessage
     * @param array $errors
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($errorMessage, array $errors = [], int $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $errorMessage,
        ];


        if(!empty($errors)){
            $response['errors'] = $errors;
        }

        return response()->json($response, $code);
    }
}
