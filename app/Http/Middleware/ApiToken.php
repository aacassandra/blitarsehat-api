<?php

namespace App\Http\Middleware;

// use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class ApiToken
{
    public function handle($request, Closure $next, $guard = null)
    {
        // return response()->json($request->token);
        if(isset($request->token) || $request->token != ''){
            $user = User::where('remember_token',$request->token)->exists();
            if($user){
                return $next($request);
            }else{
                $return = [
                    'code' => 401,
                    'message' => 'Token missmatch !',
                ];
            }
        }else{
            $return = [
                'code' => 401,
                'message' => 'Token invalid !',
            ];
        }
        return response()->json($return);

    }
}
