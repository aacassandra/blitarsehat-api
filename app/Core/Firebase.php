<?php
namespace App\Core;
use Illuminate\Support\Facades\Http;

class Firebase
{
    protected $serverKey;
    protected $endpoint = 'https://fcm.googleapis.com/fcm/send';

    function __construct( $serverKey='' ) {
        $this->serverKey = $serverKey;
    }

    private function getClient() : \Illuminate\Http\Client\PendingRequest
    {
        return Http::withHeaders( [ 'Authorization' => "key=$this->serverKey" ] );
    }

    public function push( string $recipient, string $title=null, string $body=null, array $payload=[], int $ttl = 43200 )
    {
        $payload = [
            'to'            => $recipient, // string token /topics/{topic} atau token klien
            'notification'  => [
                'body'  => $body,
                'title' => $title,
                'icon'  => null,//"local icon: misal images/logo_single.png",
                'sound' => 'default',
                #'click_action' => "beli-barang",
            ],
            'data'=> $payload,
            'time_to_live' => $ttl
        ];

        return $this->getClient()->post($this->endpoint, $payload)->json();
    }

    public function getUser( string $clientToken )
    {
        $url = "https://iid.googleapis.com/iid/info/$clientToken?details=true";
        return $this->getClient()->post($url)->json();
    }

    public function subscribeTopic( string $clientToken, string $topic )
    {
        $url = "https://iid.googleapis.com/iid/v1/$clientToken/rel/topics/$topic";
        return $this->getClient()->post($url)->json();
    }

    public function unsubscribeTopic( $clientToken, $topic )
    {
        $url = "https://iid.googleapis.com/iid/v1/$clientToken/rel/topics/$topic";
        return $this->getClient()->delete($url)->json();
    }
}
