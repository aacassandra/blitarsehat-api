<?php

use Illuminate\Support\Facades\Schema;

if( !function_exists('getModel') ){
    function getModel( string $modelOrTable , $clean = false, $trait = true){
        if(!$clean && class_exists("App\Models\\$modelOrTable") ){
            $className = "App\Models\\$modelOrTable";
            return new $className;
        }elseif( Schema::hasTable( $modelOrTable ) ){
            if($trait){
                $model = new \App\Models\ApiModel;
                $model->setTable($modelOrTable);
                return $model;
            }else{
                $model = new \App\Models\ApiModelNoTrait;
                $model->setTable($modelOrTable);
                return $model;
            }
        }
        return false;
    }
}

/**
 * Mendapatkan request atau by key
 */
if( !function_exists('req') ){
    function req( string $key = null ) {
        $pairs = explode("&", !app()->request->isMethod('GET') ? file_get_contents("php://input") : (@$_SERVER['QUERY_STRING']??@$_SERVER['REQUEST_URI']));
        $data = (object)[];
        foreach ($pairs as $pair) {
            $nv = explode("=", $pair);
            if(count($nv)<2) continue;
            $name = urldecode($nv[0]);
            $value = urldecode($nv[1]);
            $data->$name = $value;
        }

        if($key!==null){
            if( Str::contains($key, '%') ){
                $key = str_replace( '%', '', $key );
                $newData = [];
                foreach((array)$data as $keyArr=>$dt){
                    if( Str::startsWith( $keyArr, $key ) ){
                        $newData[$keyArr] = $dt;
                    }

                }
                $data = (object) $newData;
            }else{
                return isset($data->$key)? $data->$key : null;
            }
        }
        return $data;
    }

    if( !function_exists('arr2json') ){
        function arr2json($arr)
        {
            return json_decode(json_encode($arr));
        }
    }

    if( !function_exists('json2arr') ){
        function json2arr($json)
        {
            return json_decode(json_encode($json), true);
        }
    }

    if ( !function_exists('removeSpecialChar') ) {
        function removeSpecialChar($string) {
            $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

            return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        }
    }
}
