<?php

namespace App\Helpers;
use Illuminate\Support\Str;

/**
 * Class untuk simple generate decrypt dan encryption
 */
class Cryptor {

    protected $userId;
    protected $token;

    function __construct(){
        $this->userId   = \Auth::user()->getOriginal('id') ?? null;
        $this->token  = \Auth::user()->token()->id ?? null;
    }

    public function encrypt( string|int $id ){
        try{
            $tokenLength = strlen($this->token);

            $rand = random_int(1, $tokenLength<4?4:$tokenLength-4);

            $randString = substr($this->token, $rand, 4);
            $intRandString = filter_var($randString, FILTER_SANITIZE_NUMBER_INT);

            if(!is_numeric($intRandString)){
                $intRandString = 0;
            }
            $encrypted = $randString. ($id+$this->userId+$intRandString);
            return Str::substrReplace( $encrypted, '_', random_int(0,4), 0);
        }catch(\Exception $e){
            return null;
        }
    }

    public function decrypt( $encrypted ){
        $encrypted = Str::replace( "_", "", $encrypted );
        $tokenLength = strlen($this->token);
        try{
            $randString = substr($encrypted, 0, 4);

            if( !Str::contains( $this->token, $randString ) ){
                return null;
            }
            $intRandString = filter_var($randString, FILTER_SANITIZE_NUMBER_INT);
            if(!is_numeric($intRandString)){
                $intRandString = 0;
            }
            $angka = Str::replace($randString, '', $encrypted);
            if(!$angka){
                return null;
            }
            return $angka-$intRandString-$this->userId;
        }catch(\Exception $e){
            return null;
        }
    }
}
