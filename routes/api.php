<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//API LOGIN, REGISTER, LOGOUT
Route::post('signin', [AuthController::class, 'login']);
Route::post('signup', [AuthController::class, 'register']);
Route::post('signout', [AuthController::class, 'logout'])->middleware('auth:api');
Route::post('forgot-password', [AuthController::class, 'forgot_password']);

Route::get('signup-verification/{otp_code}', [AuthController::class, 'signup_verification']);
Route::post('signup/resend-otp', [AuthController::class, 'signup_resend_otp']);
Route::post('forgot-password-verification/{otp_code}', [AuthController::class, 'forgot_password_verification']);

Route::middleware('auth:api')->group(function () {
    //API GET AUTH USER
    Route::get('me', [AuthController::class, 'me']);
    //  API REQUEST ALL
    Route::get( "v1/{parent}/{parent_id?}/{detail?}/{detail_id?}/{subdetail?}/{subdetail_id?}", [ ApiController::class, 'index'] );
    Route::post( "v1/{parent}/{parent_id?}/{detail?}/{detail_id?}/{subdetail?}/{subdetail_id?}", [ ApiController::class, 'index'] );
    Route::put( "v1/{parent}/{parent_id?}/{detail?}/{detail_id?}/{subdetail?}/{subdetail_id?}", [ ApiController::class, 'index'] );
    Route::delete( "v1/{parent}/{parent_id?}/{detail?}/{detail_id?}/{subdetail?}/{subdetail_id?}", [ ApiController::class, 'index'] );

    // for handle upload file
    Route::post('/upload', function(Request $request){
        $validator = Validator::make($request->all(),[
            'file'=>'required|file|mimes:jpeg,png,jpg,svg,pdf,doc,docx,xls,xlsx|max:2500'
        ]);

        if( $validator->fails() ){
            return response()->json([
                'message'=>"File must be exists",
                'errors'=>$validator->errors()->all()
            ], 422);
        }
        try{
            $file = $request->file;
            // $subFolder = $request->folder??'files';
            $subFolder = $request->folder??'files';
            $path = "public/uploads/$subFolder";
            $filename = $file->getClientOriginalName();
            $stored = $file->storeAs( $path, $filename );
        }catch(\Exception $e){
            return response()->json([
                'message' => "Failed when trying to Store File",
                'errors' => [
                    $e->getMessage()
                ]
            ], 422);
        }
        return asset( str_replace( 'public', 'storage', $stored ) );
    });
    route::get("test",[ ApiController::class, 'test']);
});
Route::get( "v1", [ ApiController::class, 'index'] );

