<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default Schema
    |--------------------------------------------------------------------------
    |
    | Konfigurasi ini bertujuan untuk memberi tahu pada sistem kami
    | Jika suatu table tidak di daftarkan ke konfigurasi schema
    | maka sistem akan secara otomatis menggunakan nama schema dibawah ini
    |
    */
    'default' => env('DB_SCHEMA', 'cs'),

    /*
    |--------------------------------------------------------------------------
    | Markdown DB Schema of Table Settings
    |--------------------------------------------------------------------------
    |
    | Konfigurasi ini bertujuan untuk memberi tahu pada sistem kami
    | agar sistem mengetahui, di schema mana suatu table berada
    | Dengan adanya ini, kita tidak perlu men define nama schema di tiap file model dan file migration
    | kalian hanya perlu mendaftarkan nama table disini
    |
    */

    'schema' => [
        'cs' => [
            'ql_m_user_d_asuransi',
            'ql_m_user_d_document',
            'ql_m_user_d_keluraga',
            'ql_m_user',
            'oauth_user',
            'users'
        ]
    ],

];
