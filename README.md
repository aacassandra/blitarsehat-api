<img align="right" width="55" style="margin-top: 33px" src="https://user-images.githubusercontent.com/29236058/192671822-3685e0e0-2081-491c-b488-71fec89c49ee.png" />

# Blitarsehat

### Server Specification

-   Language PHP 8.1
-   Framework Laravel 9
-   Database Postgresql 14 Ke atas (stable)
-   Webserver: apache2, nginx, litespeed, httpserver
-   Composer versi 2 ke atas
-   NAS Protocol: FTP, SFTP, SSH, HTTP

### Developer Information

#### First Clone
Ketika pertama kali meng cloning repository blitarsehat ini
- composer install
- php artisan migrate
- php artisan passport:install

#### Aturan Commit

Untuk setiap commit ke repo. Anda diwajibkan mengikuti standar tata cara commit. <br/>
<br/>Contoh dunia nyata dapat terlihat seperti ini:
-   chore: run tests on travis ci
-   fix(server): send cors headers
-   feat(blog): add comment section

Jenis umum commitlint yang tersedia:
-   build
-   ci
-   chore
-   docs
-   feat
-   fix
-   perf
-   refactor
-   revert
-   style
-   test
