<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ql_m_faskes', function (Blueprint $table) {
            $table->id();
            $table->string('code',100)->unique();
            $table->string('name',100);
            $table->string('address_provinsi',30);
            $table->string('address_kota',30);
            $table->string('address_kecamatan',30)->nullable();
            $table->string('address_kelurahan',30)->nullable();
            $table->string('address_kodepos',10)->nullable();
            $table->string('address',255);
            $table->string('tipe',20)->default('rumah sakit');
            $table->string('phone',50)->nullable();
            $table->string('long',255)->nullable();
            $table->string('lat',12)->nullable();
            $table->string('note',255)->nullable();
            $table->string('status',12)->default('ACTIVE');
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->string('deleted_by',255)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ql_m_faskes');
    }
};
