<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('set_mas_gen', function (Blueprint $table) {
            $table->id();
            $table->string('kode')->nullable();
            $table->string('group')->nullable();
            $table->string('key')->nullable();
            $table->text('value_1')->nullable();
            $table->text('value_2')->nullable();
            $table->text('value_3')->nullable();
            $table->boolean('active_flag')->default(true);
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->string('deleted_by',255)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('set_mas_gen');
    }
};
