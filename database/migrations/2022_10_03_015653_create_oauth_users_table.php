<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth_user', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unique();
            $table->string('nik')->unique();
            $table->string('username',255)->unique();
            $table->string('name',50)->nullable();
            $table->string('email',50)->nullable();
            $table->string('phone',50)->nullable();
            $table->string('password',255);
            $table->string('role',10);
            $table->string('remember_token',255)->unique()->nullable();
            $table->string('status',12)->default("ACTIVE");
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->string('deleted_by',255)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth_users');
    }
};
