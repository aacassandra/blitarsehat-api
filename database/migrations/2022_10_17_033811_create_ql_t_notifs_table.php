<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ql_t_notif', function (Blueprint $table) {
            $table->id();
            $table->string('jenis', 20)->nullable();
            $table->bigInteger('user_id');
            $table->string('navigate_to', 255)->nullable();
            $table->string('title', 30);
            $table->string('content', 255);
            $table->string('image_url', 255)->nullable();
            $table->string('status', 12)->default('PENDING');
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->string('deleted_by',255)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ql_t_notif');
    }
};
