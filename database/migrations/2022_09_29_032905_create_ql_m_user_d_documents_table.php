<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ql_m_user_d_document', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ql_m_user_id');
            $table->string('jenis',15);
            $table->string('no_doc',50);
            $table->date('date_active')->nullable();
            $table->boolean('is_permanent')->nullable()->default(false);
            $table->string('image_url',255);
            $table->string('status',12);
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ql_m_user_d_documents');
    }
};
