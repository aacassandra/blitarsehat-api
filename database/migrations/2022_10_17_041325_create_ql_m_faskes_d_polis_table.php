<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ql_m_faskes_d_poli', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ql_m_faskes_id');
            $table->bigInteger('poli_id');
            $table->string('note')->nullable();
            $table->string('status',12)->default('ACTIVE');
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ql_m_faskes_d_poli');
    }
};
