<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ql_t_payment', function (Blueprint $table) {
            $table->id();
            $table->string('jenis',20);
            $table->string('bank',20)->nullable();
            $table->decimal('tarif',20,4);
            $table->string('unique_number',100);
            $table->datetime('published_at');
            $table->datetime('expired_at');
            $table->datetime('paid_at')->nullable();
            $table->bigInteger('user_id');
            $table->string('purpose',255);
            $table->string('note',20)->nullable();
            $table->string('status',12)->default('ACTIVE');
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->string('deleted_by',255)->nullable();
            $table->string('approved_by',20)->nullable();
            $table->datetime('approved_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ql_t_payment');
    }
};
