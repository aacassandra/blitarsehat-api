<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ql_m_user', function (Blueprint $table) {
            $table->id();
            $table->string('nik')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('role', 20)->nullable();
            $table->string('password', 255)->nullable();
            $table->string('username')->unique();
            $table->string('name', 100)->nullable();
            $table->string('name_prefix', 50)->nullable();
            $table->string('name_suffix', 50)->nullable();
            $table->string('gelar_prefix', 50)->nullable();
            $table->string('gelar_suffix', 50)->nullable();
            $table->integer('instalasi_id')->nullable();
            $table->string('gender', 50)->nullable();
            $table->string('birth_place', 30)->nullable();
            $table->date('birth_date')->nullable();
            $table->string('no_rm', 100)->nullable();
            $table->string('no_passport', 100)->nullable();
            $table->string('kewarganegaraan', 30)->nullable();
            $table->string('suku', 100)->nullable();
            $table->string('agama', 100)->nullable();
            $table->string('pendidikan', 100)->nullable();
            $table->string('pekerjaan', 100)->nullable();
            $table->string('status_pernikahan', 50)->default("Menikah")->nullable();
            $table->string('golongan_darah', 2)->nullable();
            $table->string('bahasa', 100)->default("Indonesia")->nullable();
            $table->string('address', 255)->nullable();
            $table->string('address_rt', 4)->nullable();
            $table->string('address_rw', 4)->nullable();
            $table->string('address_provinsi', 30)->nullable();
            $table->string('address_kota', 30)->nullable();
            $table->string('address_kecamatan', 30)->nullable();
            $table->string('address_kelurahan', 30)->nullable();
            $table->string('address_kodepos', 10)->nullable();
            $table->string('status', 12)->default("PENDING")->nullable();

            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->string('deleted_by',255)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ql_m_users');
    }
};
