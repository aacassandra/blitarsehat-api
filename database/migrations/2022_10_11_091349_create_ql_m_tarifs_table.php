<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ql_m_tarif', function (Blueprint $table) {
            $table->id();
            $table->string('jenis',20)->nullable();
            $table->string('tipe',20)->nullable();
            $table->decimal('tarif',20,4)->nullable();
            $table->bigInteger('dokter_id')->nullable();
            $table->string('note')->nullable();
            $table->string('status',12)->default('ACTIVE');
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->string('deleted_by',255)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ql_m_tarif');
    }
};
