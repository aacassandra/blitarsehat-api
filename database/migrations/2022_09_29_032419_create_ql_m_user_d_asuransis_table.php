<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ql_m_user_d_asuransi', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ql_m_user_id');
            $table->string('name',50);
            $table->string('no_card',50);
            $table->string('cara_bayar',20);
            $table->string('grade',20);
            $table->string('image_url',255);
            $table->string('status',12);
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ql_m_user_d_asuransi');
    }
};
