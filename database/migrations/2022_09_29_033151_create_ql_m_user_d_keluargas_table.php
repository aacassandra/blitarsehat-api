<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ql_m_user_d_keluarga', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ql_m_user_id');
            $table->string('name',50);
            $table->string('hubungan',20);
            $table->string('status_keluarga',20);
            $table->string('address',255);
            $table->string('phone',25)->nullable();
            $table->string('pekerjaan',100)->nullable();
            $table->string('status',12);
            $table->string('created_by',255)->nullable();
            $table->string('updated_by',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ql_m_user_d_keluarga');
    }
};
