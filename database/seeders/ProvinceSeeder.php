<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('set_mas_gen')->insert([
            ['kode'=>'11', 'group'=>'PROVINSI','value_1'=>'ACEH'],
            ['kode'=>'12', 'group'=>'PROVINSI','value_1'=>'SUMATERA UTARA'],
            ['kode'=>'13', 'group'=>'PROVINSI','value_1'=>'SUMATERA BARAT'],
            ['kode'=>'14', 'group'=>'PROVINSI','value_1'=>'RIAU'],
            ['kode'=>'15', 'group'=>'PROVINSI','value_1'=>'JAMBI'],
            ['kode'=>'16', 'group'=>'PROVINSI','value_1'=>'SUMATERA SELATAN'],
            ['kode'=>'17', 'group'=>'PROVINSI','value_1'=>'BENGKULU'],
            ['kode'=>'18', 'group'=>'PROVINSI','value_1'=>'LAMPUNG'],
            ['kode'=>'19', 'group'=>'PROVINSI','value_1'=>'KEPULAUAN BANGKA BELITUNG'],
            ['kode'=>'21', 'group'=>'PROVINSI','value_1'=>'KEPULAUAN RIAU'],
            ['kode'=>'31', 'group'=>'PROVINSI','value_1'=>'DKI JAKARTA'],
            ['kode'=>'32', 'group'=>'PROVINSI','value_1'=>'JAWA BARAT'],
            ['kode'=>'33', 'group'=>'PROVINSI','value_1'=>'JAWA TENGAH'],
            ['kode'=>'34', 'group'=>'PROVINSI','value_1'=>'DI YOGYAKARTA'],
            ['kode'=>'35', 'group'=>'PROVINSI','value_1'=>'JAWA TIMUR'],
            ['kode'=>'36', 'group'=>'PROVINSI','value_1'=>'BANTEN'],
            ['kode'=>'51', 'group'=>'PROVINSI','value_1'=>'BALI'],
            ['kode'=>'52', 'group'=>'PROVINSI','value_1'=>'NUSA TENGGARA BARAT'],
            ['kode'=>'53', 'group'=>'PROVINSI','value_1'=>'NUSA TENGGARA TIMUR'],
            ['kode'=>'61', 'group'=>'PROVINSI','value_1'=>'KALIMANTAN BARAT'],
            ['kode'=>'62', 'group'=>'PROVINSI','value_1'=>'KALIMANTAN TENGAH'],
            ['kode'=>'63', 'group'=>'PROVINSI','value_1'=>'KALIMANTAN SELATAN'],
            ['kode'=>'64', 'group'=>'PROVINSI','value_1'=>'KALIMANTAN TIMUR'],
            ['kode'=>'65', 'group'=>'PROVINSI','value_1'=>'KALIMANTAN UTARA'],
            ['kode'=>'71', 'group'=>'PROVINSI','value_1'=>'SULAWESI UTARA'],
            ['kode'=>'72', 'group'=>'PROVINSI','value_1'=>'SULAWESI TENGAH'],
            ['kode'=>'73', 'group'=>'PROVINSI','value_1'=>'SULAWESI SELATAN'],
            ['kode'=>'74', 'group'=>'PROVINSI','value_1'=>'SULAWESI TENGGARA'],
            ['kode'=>'75', 'group'=>'PROVINSI','value_1'=>'GORONTALO'],
            ['kode'=>'76', 'group'=>'PROVINSI','value_1'=>'SULAWESI BARAT'],
            ['kode'=>'81', 'group'=>'PROVINSI','value_1'=>'MALUKU'],
            ['kode'=>'82', 'group'=>'PROVINSI','value_1'=>'MALUKU UTARA'],
            ['kode'=>'91', 'group'=>'PROVINSI','value_1'=>'PAPUA BARAT'],
            ['kode'=>'94', 'group'=>'PROVINSI','value_1'=>'PAPUA'],
        ]);
    }
}
