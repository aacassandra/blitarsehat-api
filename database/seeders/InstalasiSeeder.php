<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class InstalasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ql_m_instalasi')->insert([
            ['code' => 'I12', 'name' => 'LOGISTIK'],
            ['code' => 'I11', 'name' => 'UNIT PENGADAAN'],
            ['code' => 'I10', 'name' => 'INSTALASI SISTEM INFORMASI RUMAH SAKIT'],
            ['code' => 'I09', 'name' => 'INSTALASI STERILISASI SENTRAL'],
            ['code' => 'I08', 'name' => 'INSTALASI SANITASI LINGKUNGAN'],
            ['code' => 'I07', 'name' => 'INSTALASI PEMELIHARAAN SARANA'],
            ['code' => 'I02', 'name' => 'INSTALASI GIZI'],
            ['code' => 'I01', 'name' => 'INSTALASI FARMASI'],
            ['code' => 'I04', 'name' => 'LABORATORIUM'],
            ['code' => 'I03', 'name' => 'INSTALASI GAWAT DARURAT'],
            ['code' => 'I05', 'name' => 'RAWAT INAP'],
            ['code' => 'I06', 'name' => 'RAWAT JALAN'],
        ]);
    }
}
