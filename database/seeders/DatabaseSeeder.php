<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // InstalasiSeeder::class,
            // ProvinceSeeder::class,
            // CitySeeder::class,
            // DistrictSeeder::class,
            // Village1Seeder::class,
            // Village2Seeder::class,
            // Village3Seeder::class,
            // Village4Seeder::class,
            // Village5Seeder::class,
            // Village6Seeder::class,
            // Village7Seeder::class,
            // Village8Seeder::class,
        ]);
    }
}
